#! /usr/bin/env sh

# key repeat rate
xset r rate 250 35

# compose key on r-alt
setxkbmap -option compose:ralt

# display power timeouts
xset dpms 300 300 0

# desktop wallpaper
$HOME/.fehbg
