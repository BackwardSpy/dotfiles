#! /usr/bin/env python3
import multiprocessing
import sys
import typing as t
from functools import partial
from pathlib import Path
from tempfile import TemporaryDirectory

from plumbum import local

try:
    MODE = sys.argv[1]
except IndexError:
    print(f"usage: {sys.argv[0]} MODE")
    sys.exit(1)

PLATFORM = sys.platform

NPROC = multiprocessing.cpu_count()

PREFIX = Path("~/.local").expanduser()
BIN = PREFIX / "bin"
SRC = PREFIX / "src"


def verify_mode():
    allowable_modes = ["deb"]
    if MODE not in allowable_modes:
        raise NotImplementedError(
            f"this script does not support running in {MODE} mode."
        )


def info(msg: str) -> None:
    print(f" - {msg}")


def subinfo(msg: str) -> None:
    print(f"   - {msg}")


def apt_install(*packages) -> None:
    from plumbum.cmd import sudo, apt_get

    cmd = sudo[apt_get["install", packages]]
    cmd()


def cargo_install(package: str) -> None:
    from plumbum.cmd import cargo

    cargo("install", package, "--root", PREFIX)


def make_links(*packages) -> None:
    from plumbum.cmd import stow

    for package in packages:
        subinfo(f"linking {package}")
        stow(package)


def get_src(package_name: str, repo_url: str) -> Path:
    from plumbum.cmd import git

    src_dir = SRC / package_name

    if not src_dir.exists():
        subinfo("cloning source repo")
        git("clone", repo_url, src_dir)
    else:
        subinfo("updating source repo")
        with local.cwd(src_dir):
            git("pull")

    return src_dir


def install_pipx() -> None:
    from plumbum.cmd import curl, python3

    cmd = (
        curl["https://raw.githubusercontent.com/cs01/pipx/master/get-pipx.py"] | python3
    )
    cmd()


def install_pipenv() -> None:
    from plumbum.cmd import pipx

    pipx("install", "pipenv")


def install_fish() -> None:
    if MODE != "deb":
        raise NotImplementedError(f"i don't know how to install fish in {MODE} mode!")

    from plumbum.cmd import cmake, make

    subinfo("installing requirements")
    apt_install("cmake", "libncurses5")

    src_dir = get_src("fish", "https://github.com/fish-shell/fish-shell.git")
    build_dir = src_dir / "build"
    binary = build_dir / "fish"

    subinfo("building")
    build_dir.mkdir(exist_ok=True)
    with local.cwd(build_dir):
        cmake("..")
        make("-j", NPROC)

    subinfo("linking fish binary")
    (BIN / "fish").symlink_to(binary)


def install_cargo() -> None:
    from plumbum.cmd import curl, sh

    cmd = curl["https://sh.rustup.rs", "-sSf"] | sh["-s", "--", "-y"]
    cmd()


def install_stow() -> None:
    from plumbum.cmd import wget, tar, make

    archive = "stow-latest.tar.gz"
    pkg = f"https://ftp.gnu.org/gnu/stow/{archive}"

    src_dir = SRC / "stow"
    src_dir.mkdir(exist_ok=True)

    with TemporaryDirectory() as d:
        tempdir = Path(d)

        subinfo("downloading tarball")
        wget(pkg, "-P", tempdir)

        subinfo("extracting tarball")
        tar("xzf", tempdir / archive, "-C", src_dir, "--strip", "1")

    with local.cwd(src_dir):
        prefix = src_dir / "build"
        prefix.mkdir(exist_ok=True)

        subinfo("building")
        local["./configure"](f"--prefix={prefix}")
        make("install")

    (BIN / "stow").symlink_to(prefix / "bin" / "stow")


def main() -> None:
    verify_mode()

    print(f"stage 1 running in {MODE} mode with {NPROC} cores")

    if not BIN.exists():
        info(f"creating {BIN}")
        BIN.mkdir()

    if not SRC.exists():
        info(f"creating {SRC}")
        SRC.mkdir()

    steps: t.Dict[Path, t.Callable] = {
        Path("~/.cargo/bin/cargo").expanduser(): install_cargo,
        BIN / "pipx": install_pipx,
        BIN / "pipenv": install_pipenv,
        BIN / "fish": install_fish,
        BIN / "exa": partial(cargo_install, "exa"),
        BIN / "rg": partial(cargo_install, "ripgrep"),
        BIN / "bat": partial(cargo_install, "bat"),
        BIN / "stow": install_stow,
    }

    for marker, install_fn in steps.items():
        if not marker.exists():
            info(f"installing {marker.name}")
            install_fn()

            if marker.exists():
                subinfo(f"{marker.name} installed successfully")
            else:
                subinfo(
                    f"warning: ran {marker.name} installation but {marker} does not exist"
                )
        else:
            info(f"{marker.name} already installed")

    info("linking dotfiles")
    make_links("emacs", "fish", "i3")


if __name__ == "__main__":
    main()
