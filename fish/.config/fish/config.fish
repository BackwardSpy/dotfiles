if status --is-login
    set -gx SHELL (which fish)
    set -gx EDITOR (which vim)
    set -gx VISUAL (which emacs)
    set -gx PIPENV_SHELL_FANCY true
    set -gx PYENV_ROOT $HOME/.pyenv
    set -gx WORKON_HOME $HOME/.virtualenvs
    set -gx FISH $HOME/.config/fish

    set paths \
    $HOME/.local/bin \
    $HOME/.local/npm/bin \
    $HOME/.linuxbrew/bin \
    $HOME/.linuxbrew/sbin \
    $PYENV_ROOT/bin \
    $HOME/.gem/ruby/2.3.0/bin \
    $HOME/.cargo/bin

    for path in $paths
        if [ -d $path ]
            set -gx PATH $path $PATH
        end
    end
end

if [ -f ~/.local/bin/exa ]
    function ls
        exa -lF --git --group-directories-first --time-style long-iso --colour-scale $argv
    end
end

function _maybe_source
    if [ -f $argv ]
        source $argv
    end
end

# Kubernetes Tools fish completion start
_maybe_source /home/chris/.config/fish/completions/kubernetes-tools.fish

functions -e _maybe_source
